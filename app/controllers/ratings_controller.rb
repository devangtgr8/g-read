class RatingsController < ApplicationController

	def show
    @rating = Review.find(params[:id])
  end

  def new
    @rating = Review.new
  end

  def create
    @rating = Rating.create(review_id: @review.id, user_id: @current_user.id, score: 0)
  end

   def update
    @rating = Rating.find(params[:id])
    @rating = @rating.review
    if @rating.update_attributes(score: params[:score])
      respond_to do |format|
        format.js
      end
    end
  end

  def index
    @rating = Review.paginate(page: params[:page])
  end

  def average_rating
  ratings.sum(:score) / ratings.size
  end

  private

    def micropost_params
      params.require(:review).permit(:content)
    end
end