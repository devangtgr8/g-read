class BooksController < ApplicationController

  before_action :signed_in_user

  def show
    @book = Book.find(params[:id])
    @reviews = @book.reviews.paginate(page: params[:page])
    @review = Review.new
  end

  def new
    @book = Book.new
  end

  def create
    @book = Book.new(book_params)
      if @book.save
        redirect_to @book
      else
        render 'new'
      end
  end

  def index
    @books = Book.paginate(page: params[:page])
  end



  private

    def book_params
      params.require(:book).permit(:title, :author, :isbn)
    end


    # Before filters

    def signed_in_user
      redirect_to new_session_path, notice: "Please sign in." unless signed_in?
    end

    def correct_user
      @user = User.find(params[:id])
      redirect_to(root_url) unless current_user?(@user)
    end
end
