class ReviewsController < ApplicationController
  before_action :signed_in_user

  def show
    @review = Review.find(params[:id])
  end

  def new
     @book = Book.find_using_slug(params[:book_id])
     @review = @book.reviews.build
     @review.user_id = current_user.id if user_signed_in?
  end

  def create

    #@review = Review.new(review_params)
    @book = Book.find(params[:book_id])
     @review = @book.reviews.build(review_params)
     @review.user_id = current_user.id if user_signed_in?
    if @review.save
      flash[:success] = "review created!"
      redirect_to root_url
    else
      render 'static_pages/home'
    end
  end

  def update
   @book = Book.find(params[:book_id])
   @review = @book.reviews.find(params[:id])
    @review = Review.find(params[:id])

    respond_to do |format|
      if @review.update_attributes(params(review_params))
        format.html { redirect_to(book_review_path(@review.book, @review), :notice => 'review was successfully updated.') }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @review.errors, :status => :unprocessable_entity }
      end
    end
  end

def edit
    @review = Review.find(params[:id])
    3.times { @review.review_items.build }
  end

  def index
    @review = Review.paginate(page: params[:page])
  end

  def average_rating
  ratings.sum(:score) / ratings.size
  end

  def destroy

  end

  private

    def review_params
      params.require(:review).permit(:content)
    end
end

