class Review < ActiveRecord::Base
  belongs_to :user
  belongs_to :book
  default_scope -> { order('created_at DESC') }
  validates :user_id, presence: true
  validates :content, presence: true
  validates :book_id, presence: true

  attr_accessible :content, :user_id, :book_id
end
