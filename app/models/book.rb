class Book < ActiveRecord::Base
	attr_accessible :author, :isbn, :title

	has_many :reviews
	has_many :ratings
	accepts_nested_attributes_for :reviews
end
