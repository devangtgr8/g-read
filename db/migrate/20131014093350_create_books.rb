class CreateBooks < ActiveRecord::Migration
  def change
    create_table :books do |t|
      t.string :title
      t.string :author
      t.integer :isbn

      t.timestamps
    end
    add_index :reviews, [:book_id, :user_id, :created_at]
  end
end
