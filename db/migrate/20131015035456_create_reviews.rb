class CreateReviews < ActiveRecord::Migration
  def change
    create_table :reviews do |t|
      t.string :content
      t.references :user, index: true
      t.references :book, index: true

      t.timestamps
    end
    add_index :reviews, [:book_id, :user_id, :created_at]
  end
end
