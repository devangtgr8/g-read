namespace :db do
  desc "Fill database with sample data"
  task populate: :environment do
    User.create!(username: "Devang",
                 email: "devangtgr8@gmail.com",
                 password: "abcdefghi",
                 password_confirmation: "abcdefghi")
    99.times do |n|
      name  = Faker::Name.name
      email = "devang-#{n+1}@gmail.com"
      password  = "password"
      User.create!(username: name,
                   email: email,
                   password: password,
                   password_confirmation: password)
    end
  end
end